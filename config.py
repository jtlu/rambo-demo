class DataConfig(object):
    data_path = "/mounted_files/data/"
    data_name = "preprocessed"
    height = 192
    width = 256
    num_channels = 2


class TestConfig(DataConfig):
    model_path = "checkpoints/final_model.hdf5"
    angle_train_mean = -0.004179079


class VisualizeConfig(object):
    pred_path = "submissions/final_model.txt"
    true_path = "data/true_angles.csv"
    img_path = "/mounted_files/images/*.jpg"
