#!/bin/bash

pip install pygame
pip install pandas

mkdir -p data/generated

docker image inspect rambo-demo:latest > /dev/null
if [ 0 -ne $? ]; then
  docker build -t rambo-demo .
  if [ 0 -ne $? ]; then
    echo "Docker error. Consider escalating privileges."
    exit 1
  fi
fi

# Pre-process images for prediction
docker run --runtime=nvidia -v `pwd`:/mounted_files rambo-demo /mounted_files/prepare.py

# Predict
docker run --runtime=nvidia -v `pwd`:/mounted_files rambo-demo /mounted_files/predict.py

python2 visualize.py