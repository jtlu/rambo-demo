FROM tensorflow/tensorflow:1.8.0-gpu
RUN pip install --upgrade pip
RUN pip install keras==2.1.6
RUN pip install scikit-image
ENTRYPOINT ["python"]