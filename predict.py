import glob
import os

import numpy as np
import pandas as pd

from config import TestConfig
from keras.models import load_model

config = TestConfig()

if not os.path.exists("/mounted_files/data/generated/test.npy"):
    print "Please run prepare.py first"
    exit()

print "Loading model..."
model = load_model("/mounted_files/data/final_model.hdf5")
X_train_mean = np.load("/mounted_files/data/X_train_mean.npy")

print "Reading test data..."
X_test = np.load("/mounted_files/data/generated/test.npy")
X_test = X_test.astype('float32')
X_test -= X_train_mean
X_test /= 255.0

print "Predicting..."
preds = model.predict(X_test)
preds = preds[:, 0]
dummy_preds = np.repeat(config.angle_train_mean, config.num_channels)

# Tao: because the prediction is delayed by a frame, I guess
preds = np.concatenate((dummy_preds, preds))

# Join predictions with frame_ids
filenames = glob.glob("/mounted_files/images/*.jpg")
filenames = sorted(filenames)
frame_ids = [f.replace(".jpg", "").replace(
    "/mounted_files/images/", "") for f in filenames]
pd.DataFrame({"frame_id": frame_ids, "steering_angle": preds}
             ).to_csv("/mounted_files/data/generated/predicted_angles.csv", index=False, header=True)

print "Done!"
