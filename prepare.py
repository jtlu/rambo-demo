import glob
import sys

import numpy as np
import pandas as pd
from matplotlib.colors import rgb_to_hsv
from skimage.exposure import rescale_intensity

from config import DataConfig
from keras.preprocessing.image import img_to_array, load_img

# Prepares test data for prediction


def make_hsv_grayscale_diff_data(num_channels=2):
    num_rows = len(filenames)
    print "Processing", num_rows, "images"
    X = np.zeros((num_rows - num_channels, row,
                  col, num_channels), dtype=np.uint8)
    for i in range(num_channels, num_rows):
        if i % 100 == 0:
            progress = i / float(num_rows)
            filled_bars = int(progress * 100)
            num_bars = filled_bars
            sys.stdout.write('\r')
            sys.stdout.write("[%-100s] %d%%" % ('=' * num_bars, filled_bars))
            sys.stdout.flush()
        for j in range(num_channels):
            path0 = filenames[i - j - 1]
            path1 = filenames[i - j]
            img0 = load_img(path0, target_size=(row, col))
            img1 = load_img(path1, target_size=(row, col))
            img0 = img_to_array(img0)
            img1 = img_to_array(img1)
            img0 = rgb_to_hsv(img0)
            img1 = rgb_to_hsv(img1)
            img = img1[:, :, 2] - img0[:, :, 2]
            img = rescale_intensity(
                img, in_range=(-255, 255), out_range=(0, 255))
            img = np.array(img, dtype=np.uint8)

            X[i - num_channels, :, :, j] = img
    sys.stdout.write('\r')
    sys.stdout.flush()
    return X


if __name__ == "__main__":
    config = DataConfig()
    data_path = config.data_path
    row, col = config.height, config.width

    filenames = glob.glob("/mounted_files/images/*.jpg".format(data_path))
    filenames = sorted(filenames)
    X_test = make_hsv_grayscale_diff_data()
    np.save("/mounted_files/data/generated/test", X_test)

    print "Done"
