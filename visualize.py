import glob
import math

import numpy as np
import pandas as pd

import pygame
from config import VisualizeConfig

BLACK = (0,   0,   0)
WHITE = (255, 255, 255)
BLUE = (0,   0, 255)
GREEN = (0, 255,   0)
RED = (255,   0,   0)

preds = pd.read_csv("data/generated/predicted_angles.csv")
true = pd.read_csv("data/true_angles.csv")

pygame.init()
size = (640, 320)
pygame.display.set_caption("Data viewer")
screen = pygame.display.set_mode(size, pygame.DOUBLEBUF)
myfont = pygame.font.SysFont("monospace", 15)

clock = pygame.time.Clock()
squared_error = 0

for i in range(5000):
    angle = 2 * np.pi - preds["steering_angle"].iloc[i]  # radians
    true_angle = true["steering_angle"].iloc[i]  # radians

    squared_error += pow(true_angle - angle, 2)

    # Set background to be the image
    img = pygame.image.load("images/" +
                            str(preds["frame_id"].iloc[i]) + ".jpg")
    screen.blit(img, (0, 0))

    # Text
    pred_txt = myfont.render(
        "Prediction:" + str(round(angle * 57.2958, 3)), 1, (255, 255, 0))  # angle in degrees
    true_txt = myfont.render(
        "True angle:" + str(round(true_angle * 57.2958, 3)), 1, (255, 255, 0))  # angle in degrees
    screen.blit(pred_txt, (10, 280))
    screen.blit(true_txt, (10, 300))

    # Steering wheel
    radius = 50
    pygame.draw.circle(screen, WHITE, [320, 300], radius, 2)

    # Circle for true angle
    x = radius * np.cos(np.pi/2 + true_angle)
    y = radius * np.sin(np.pi/2 + true_angle)
    pygame.draw.circle(screen, WHITE, [320 + int(x), 300 - int(y)], 7)

    # Circle for predicted angle
    x = radius * np.cos(np.pi/2 + angle)
    y = radius * np.sin(np.pi/2 + angle)
    pygame.draw.circle(screen, BLACK, [320 + int(x), 300 - int(y)], 5)

    # Draw the frame
    pygame.display.flip()

print "RMSE", math.sqrt(squared_error / 5000)
